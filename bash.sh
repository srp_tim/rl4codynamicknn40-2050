#!/usr/bin/env bash
#SBATCH --job-name=symnco_20#SBATCH --output=symnco_20%j.log
#SBATCH --error=symnco_20%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python run20_symnco.py
