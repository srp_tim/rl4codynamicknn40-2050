#!/usr/bin/env bash
#SBATCH --job-name=gcn20-50
#SBATCH --output=gcn20-50%j.log
#SBATCH --error=gcn20-50%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

#WANDB_MODE=dryrun
wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python  gcn_ckpt.py


















