from rl4co.envs import TSPEnv
from rl4co.models.zoo import AttentionModel
import numpy as np
import torch
from rl4co.envs.tsp import TSPEnv
from rl4co.models.zoo import AttentionModel
from rl4co.utils.trainer import RL4COTrainer
import torch
from rl4co.models.nn.graph.gcn import GCNEncoder
from rl4co.models.nn.graph.mpnn import MessagePassingEncoder

import wandb
wandb.init(project="rl4co")
from lightning.pytorch.loggers import WandbLogger
logger = WandbLogger(project="rl4co", name = "attn_gcn")

gcn_encoder = GCNEncoder(
    env_name='tsp',
    embedding_dim=128,
    num_nodes=100,
    num_layers=3,
)

#env = TSPEnv()
#model = AttentionModel(env,
#                train_data_size=100_000,
#                baseline="rollout",
#                optimizer_kwargs={'lr': 1e-4},
#                policy_kwargs={
#                'encoder': gcn_encoder # gcn_encoder or mpnn>         
#                })

np.random.seed(1234)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = AttentionModel.load_from_checkpoint("rl4co/n159n85j/checkpoints/epoch=99-step=19600.ckpt", strict=False)
env = TSPEnv(num_loc=20)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(10000)
dataloader = model._dataloader(new_dataset, batch_size=512)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 20 nodes: ", out['reward'].mean())


model = AttentionModel.load_from_checkpoint("rl4co/n159n85j/checkpoints/epoch=99-step=19600.ckpt", strict=False)
env = TSPEnv(num_loc=50)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(10000)
dataloader = model._dataloader(new_dataset, batch_size=128)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 50 nodes: ", out['reward'].mean())

model = AttentionModel.load_from_checkpoint("rl4co/n159n85j/checkpoints/epoch=99-step=19600.ckpt", strict=False)
env = TSPEnv(num_loc=100)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(10000)
dataloader = model._dataloader(new_dataset, batch_size=128)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 100 nodes: ", out['reward'].mean())

model = AttentionModel.load_from_checkpoint("rl4co/n159n85j/checkpoints/epoch=99-step=19600.ckpt", strict=False)
env = TSPEnv(num_loc=500)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(1000)
dataloader = model._dataloader(new_dataset, batch_size=8)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 500 nodes: ", out['reward'].mean())

model = AttentionModel.load_from_checkpoint("rl4co/n159n85j/checkpoints/epoch=99-step=19600.ckpt", strict=False)
env = TSPEnv(num_loc=1000)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(1000)
dataloader = model._dataloader(new_dataset, batch_size=2)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 1000 nodes: ", out['reward'].mean())

wandb.finish()
