from rl4co.envs.tsp import TSPEnv
from rl4co.models.zoo import POMO
from rl4co.utils.trainer import RL4COTrainer
import torch
from rl4co.models.nn.graph.gcn import GCNEncoder
from rl4co.models.nn.graph.mpnn import MessagePassingEncoder

import wandb
wandb.init(project="rl4co")

from lightning.pytorch.loggers import WandbLogger
logger = WandbLogger(project="rl4co", name = "pomo_gat")

gcn_encoder = GCNEncoder(
    env_name='tsp',
    embedding_dim=128,
    num_nodes=100,
    num_layers=3,
)

mpnn_encoder = MessagePassingEncoder(
    env_name='tsp',
    embedding_dim=128,
    num_nodes=1000,
    num_layers=3,
)


from lightning.pytorch.callbacks import ModelCheckpoint, RichModelSummary

# Checkpointing callback: save models when validation reward improves
checkpoint_callback = ModelCheckpoint(  dirpath="checkpoints", # save to checkpoints/
                                        filename="epoch_{epoch:03d}",  # save as epoch_XXX.ckpt
                                        save_top_k=1, # save only the best model
                                        save_last=True, # save the last model
                                        monitor="val/reward", # monitor validation reward
                                        mode="max") # maximize validation reward

# Environment, Model, and Lightning Module
env = TSPEnv(num_loc=20, batch_size=128, train_file = "rl4co/data/data/tsp/tsp20_train_gen_seed1111.npz", test_file =  "rl4co/data/data/tsp/tsp20_test_seed1234.npz", val_file = "rl4co/data/data/tsp/tsp20_val_seed4321.npz")
model = POMO(env,
                optimizer_kwargs={'lr': 1e-4}
                )


# Print model summary
rich_model_summary = RichModelSummary(max_depth=3)

# Callbacks list
callbacks = [checkpoint_callback, rich_model_summary]

# Greedy rollouts over untrained model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
td_init = env.reset(batch_size=[3]).to(device)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)

# Training

trainer = RL4COTrainer(
    max_epochs=100,
    accelerator="gpu",
    logger=logger,
    callbacks=callbacks,
)


# Fit the model
trainer.fit(model)

# Test the model
trainer.test(model)

# Greedy rollouts over trained model (same states as previous plot)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)
wandb.finish()

